# Deploy do nginx através de uma pipeline

Este guia oferece um processo eficiente para implantar automaticamente um servidor web Nginx usando GitLab CI e Docker Compose. A pipeline engloba a construção da imagem Docker, possíveis testes e a implantação do Nginx com o auxílio do Docker Compose.

## Pré-requisitos

Certifique-se de ter os seguintes requisitos antes de começar:

1. Conta no GitLab.
2. Git instalado em sua máquina local.
3. Docker instalado em sua máquina local.

## Configuração do Repositório

1. Crie um novo repositório no GitLab para o projeto Nginx.
2. Clone o repositório em sua máquina local:

   ```bash
   git clone <URL_DO_REPOSITORIO_NGINX>
   cd <NOME_DO_REPOSITORIO_NGINX>
   ```

## Estrutura do Projeto

1. Estabeleça uma estrutura básica para o projeto:

   ```bash
   mkdir web
   touch web/index.html
   ```

   Adicione conteúdo ao arquivo `web/index.html` para fins de teste.

## Configuração do Docker

1. Crie dentro do diretorio web, o arquivo Dockerfile:

   ```dockerfile
   # Use a imagem base do Nginx
   FROM nginx:latest

   # Copie o conteúdo do diretório web para o diretório padrão do Nginx
   COPY ./web/index.html /usr/share/nginx/html/index.html
   ```

2. Crie um arquivo docker-compose.yml na raiz do projeto:

   ```yaml
   version: "3.1"
   services:
     web:
       restart: always
       build:
         context: .
         dockerfile: ./web/Dockerfile
       ports:
         - "3000:80"
   ```

## Configuração da Pipeline com GitLab CI

1. Crie o arquivo `.gitlab-ci.yml` na raiz do projeto:

   ```yaml
   image: docker:latest

   variables:
     DOCKER_TLS_CERTDIR: ""

   services:
     - docker:dind

   stages:
     - build
     - deploy

   build:
     stage: build
     only:
       - main
     tags:
       - main
     script:
       - docker-compose build

   deploy:
     stage: deploy
     only:
       - main
     tags:
       - main
     script:
       - docker-compose up -d
   ```

2. Execute a pipeline com os seguintes comandos:

   ```bash
   git add .
   git commit -m `<MENSAGEM_DE_COMMIT>`
   git push origin master
   ```

Acesse o GitLab, vá para o projeto, e a CI/CD será acionada automaticamente pelo push, implantando o Nginx com base nas configurações da pipeline.

**Conclusão**

Seguindo este guia, você estabeleceu uma pipeline no GitLab CI para construir, testar e implantar o Nginx automaticamente usando Docker Compose.

**Acesse a porta 3000 da sua máquina para visualizar o servidor web Nginx implantado.** 🚀
